rm -rf $HOME/.lekha
source /usr/local/bin/virtualenvwrapper.sh
echo "removing virtualenv pyt3 and its files..."
rmvirtualenv pyt3
rm -rf $HOME/.envs
echo "removing desktop icon..."
sudo rm -f /usr/share/applications/lekha_ocr_ver3.desktop
echo "Lekha ocr version 3.0 uninstalled successfully"
