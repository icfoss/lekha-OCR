#!/usr/bin/env python
# coding: utf-8

# In[15]:


import re
from collections import Counter
import pickle
import sys
import os
import argparse
import itertools
import make_word_original


# In[16]:


with open('com_fil.pkl', 'rb') as inputfile:
     WORDS = pickle.load(inputfile)


# In[17]:


letters='ആ ഇ ഉ എ ഏ ഒ ക ക്ക ക്ത ക്ഷ ഖ ഗ ഘ ങ്ക ങ്ങ ച ച്ച ജ ഞ ഞ്ഞ ട ട്ട ഠ ഡ ണ ണ്ട ണ്ണ ത ത്ത ത്ഥ ഥ ദ ദ്ദ ദ്ധ ധ ന ന്ത ന്ദ ന്ധ ന്ന ന്മ ന്റ പ പ്പ ഫ ബ ഭ മ മ്പ മ്മ യ യ്യ ര റ റ്റ ല ല്ല ള ള്ള ഴ വ ശ ഷ സ സ്സ സ്ഥ ഹ ാ ി ീ ു ൂ ൃ െ േ ് ്യ ്ര ്വ ൗ ൺ ൻ ർ ൽ ൾ ൾ'.split(' ')


# In[24]:


def P(word, N=sum(WORDS.values())): 
    
    return WORDS[word] / N

def correction1(word): 
    if not (candidates(word)):
        return(word)
    return max(candidates(word), key=P)

def candidates(word): 
    return (known([word]) or known(edits1(word)) or known(edits2(word)))

def known(words): 
    return set(w for w in words if w in WORDS)

def edits1(word):
    splits     = [(word[:i], word[i:])    for i in range(len(word) + 1)]
    deletes    = [L + R[1:]               for L, R in splits if R]
    replaces   = [L + c + R[1:]           for L, R in splits if R for c in letters]
    inserts    = [L + c + R               for L, R in splits for c in letters]
    return set(deletes + replaces + inserts)

def edits2(word): 
    return (e2 for e1 in edits1(word) for e2 in edits1(e1))


# In[26]:

#this module further improves accuracy of lekha by spell check and correction,
#spellcheck is now set as an optional feature which can be turned on and off
#spellcheck can be switched on and off by specifing flag in conf file
def correction2(wrd,beam):
    if WORDS[wrd]:
        return wrd
    if len(wrd)>14:
        return wrd
    
    class_arr=[]
    score_arr=[]
    for elm in beam:
        temp_c=[]
        temp_s=[]
        for cls,scr in elm:
            temp_c.append(cls)
            temp_s.append(scr)
        class_arr.append(temp_c)
        score_arr.append(temp_s) 
    word_probs=sorted(list(zip(list(map(lambda x: ''.join(x), list(itertools.product(*class_arr)))),list(map(lambda x: sum(x), list(itertools.product(*score_arr)))))),key=lambda p:p[1],reverse=True)[:3]

    for word,prob in word_probs:
        words=make_word_original.form_word(word)
        if WORDS[words]:
            return words
    return wrd


# In[23]:


def spellcheck(txt,spell,beam):
    if(spell==1):
        return correction1(txt)  
    elif(spell==2):
        return correction2(txt,beam)


# In[ ]:




