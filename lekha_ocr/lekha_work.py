#!/usr/bin/env python
# coding: utf-8

# In[32]:


#    Lekha Ocr version 2.0 - Convert your malayalam documents and images to editable text
#    Copyright (C) 2018 Space-kerala (Society For Promotion of Alternative Computing and Employment)

#    Lekha Ocr version 2.0 is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.

#    Lekha Ocr version 2.0 is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.

#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.

# coding: utf-8

# In[1]:


import warnings
warnings.filterwarnings('ignore')
import json
import cv2
import math
import numpy as np
from sklearn import svm
from sklearn.externals import joblib
import features
import make_word_original
from sklearn.preprocessing import normalize,Normalizer
from matplotlib import pyplot as plt
from spellcorrection import spellcheck
import pandas as pd
import re
import pickle
import itertools


# In[33]:


# In[2]:
def takecenteroid(cnt):
    M = cv2.moments(cnt)
    cX = int(M["m10"] / M["m00"])
    return int(cX)


# In[34]:


# During postprocess lekha determines if the word being processed is malayalam or english
# based on the frequency of occurence of individual letters in the word.After that, using
# predictproba it finds the most probable choice for each character based on the found word language

def postprocess(char_list,prob_list,regex):
    si=[',','.',';',':','?','(',')','[',']','{','}',',','"',"'"]
    stri=[]
    for i in range(len(char_list)):
#        if char_list[i] in si :
#            stri.append(char_list[i])
#            continue
        if (i==0 or i==len(char_list)-1) and char_list[i] in si :
            stri.append(char_list[i])
            continue
        elif(i!=0 and i!=len(char_list)-1 ):
            if(char_list[i-1] in si or char_list[i+1] in si) and char_list[i] in si :
                stri.append(char_list[i])
                continue
        for j in range(len(prob_list[i])):
            z=len(re.findall(regex,prob_list[i][j]))
            if(z>0):
                stri.append(prob_list[i][j])
                break
    return stri


# In[35]:

# reading configuration files
def lekha_run(im):
    conf = json.load(open('conf.json', 'r'))
    lekha_type=str(conf['lekha'])
    clf_path =conf['clf_path']+lekha_type
    normalizer_path=conf['norm_path']+lekha_type+'.pkl'
    spell=conf["spell"]
    lang_process=conf["postprocess"]
    string=' '
    prev_char=' '
    X_sample_n = joblib.load(normalizer_path)
    classifier = joblib.load(clf_path)


    image_h,image_w=im.shape

    # scaned documents contain glyp impressions of other side , noise , removing noise through threshholding



    img= cv2.adaptiveThreshold(im,255,cv2.ADAPTIVE_THRESH_GAUSSIAN_C,cv2.THRESH_BINARY_INV,243,50)
    #kernel to join along horizontal
    kernel_line = np.array([[0,0,0,0,0],[0,0,0,0,0],[1,1,1,1,1],[0,0,0,0,0],[0,0,0,0,0]],np.uint8)
    dilation = cv2.dilate(img,kernel_line,iterations = 6)
    #plot("sds",dilation)

    im, contours, hierarchy = cv2.findContours(dilation.copy(), cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)
    avg_height_arr=[]

    # finding parameters such as average character height, word area etc
    for cnt in contours:
        x,y,w,h= cv2.boundingRect(cnt)
        if w*h > 1000:
            avg_height_arr.append(h)

    #detect average height of a line in document which should include punctuations


    if len(avg_height_arr)<9:
        avg_height=np.mean(sorted(avg_height_arr,reverse=True))
    else:
        avg_height=np.mean(sorted(avg_height_arr,reverse=True)[2:9])
    # wordarea , minimum of 2 letters

    word_area=(avg_height*avg_height)/1.8

    kernel_chandrakala = np.array([[0,0,1,1,1],[0,0,1,1,0],[1,1,1,1,1],[0,0,0,0,0],[0,0,0,0,0]],np.uint8)
    ker_width=kernel_chandrakala.shape[1]/2

    iteration=int(math.ceil((avg_height/(1.5*ker_width))))
    dilation = cv2.dilate(dilation,kernel_line,iterations = (iteration-5))


    line_boxes=[]
    im, contours, hierarchy = cv2.findContours(dilation.copy(), cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)
    # find lines in images
    for cnt in contours:

        x,y,w,h= cv2.boundingRect(cnt)

        if (h > (avg_height/1.7)) & (w>(avg_height+(ker_width*iteration*2))):

            line_boxes.append([x,y,image_w,y+h])

    line_boxes=sorted(line_boxes,key=lambda box: box[1])

    if len(line_boxes)==0:
        print ("no lines found")

    kernel_word = np.array([[1,1,1],[1,1,1],[1,1,1],[0,0,0],[0,0,0]],np.uint8)

    # find words in image
    for box in line_boxes:

        line_image= img[box[1]:box[3],box[0]:box[2]]
        word_dilate= cv2.dilate(line_image,kernel_word,iterations = int(avg_height/10))
        w_h,w_w=word_dilate.shape


        im_w, contours_w, hierarchy_w = cv2.findContours(word_dilate.copy(), cv2.RETR_TREE, cv2.CHAIN_APPROX_SIMPLE)
        word_boxes=[]

        for cnt in contours_w:
            x,y,w,h= cv2.boundingRect(cnt)
            if h >= (w_h/2):
                w_area=cv2.contourArea(cnt)
                if w_area>word_area:
                    word_boxes.append([x,0,x+w,w_h])

        if len(word_boxes)==0:
            continue
        word_boxes=sorted(word_boxes,key=lambda w_box: w_box[0])
        # find leatters in words
        for w_box in word_boxes:
            word_cut=line_image[w_box[1]:w_box[3],w_box[0]:w_box[2]]
            im_le, contours_le, hierarchy_we = cv2.findContours(word_cut.copy(), cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)
            letters=sorted([cnt for cnt in contours_le if (cv2.contourArea(cnt)>10)],key=takecenteroid)

            char_list=[]
            prob_list=[]
            beam=[]
            mal=0
            eng=0
            num=0
            #for each found letter extract features and classify using svm
            for letter in letters:
                x_le,y_le,w_le,h_le=cv2.boundingRect(letter)

                char=word_cut[y_le:y_le+h_le,x_le:x_le+w_le]
                char_feature=np.array(features.HOG(char.copy()))
                char_feature=char_feature.reshape(1,-1)
                char_feature=X_sample_n.transform(char_feature)

                predicted_letter=classifier.predict(char_feature)
                predicted_letter=predicted_letter[0]
                if predicted_letter in ['o','0','ഠ','ം']:
                    if h_le < w_box[3]/2.25:
                        predicted_letter= 'ം'

                results = classifier.predict_proba(char_feature)[0]
                results_sorted=sorted(zip(classifier.classes_, results), key=lambda x: x[1], reverse=True)
                results_ordered_by_probability = list(map(lambda x: x[0],results_sorted))
                prob_list.append(results_ordered_by_probability)
                if(predicted_letter != 'ം'):
                    if y_le > w_box[3]/2.3:
                        if(results_ordered_by_probability.index(',') < results_ordered_by_probability.index('.')):
                            predicted_letter=','
                        else:
                            predicted_letter='.'
                char_list.append(predicted_letter)
                # probability of charater belonging to each class sorted and largest 3 selected, for shape oriented spell check
                beam.append(results_sorted[:3])

                # to find language of dectected letter
                if(len(re.findall(r"[\u0D00-\u0D7F]",predicted_letter))>0):
                    mal=mal+1
                elif(len(re.findall(r"[a-zA-z]",predicted_letter))>0):
                    eng=eng+1
                elif(len(re.findall(r"[0-9]",predicted_letter))>0):
                    num=num+1
            if(lang_process!=0):
                for i in range(len(char_list)):
                    if(prob_list[i][0]!=char_list[i]):
                        prob_list[i].insert(0, char_list[i])
                lang=max(mal,eng,num)
                if(lang==mal):
                    regex=r"[\u0D00-\u0D7F]"
                elif(lang==eng):
                    regex=r"[a-zA-z]"
                else:
                    regex=r"[0-9]"
                stri=postprocess(char_list,prob_list,regex)
            else:
                stri=char_list
            # form words based on malayalam rules
            wrd=make_word_original.form_word(stri)
            if(spell==1 or spell==2):
                string=string+spellcheck(wrd,spell,beam)
            else:
                string=string+wrd
            string=string+' '
        string=string+'\n'
    return string
