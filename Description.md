Lekha OCR converts document images to text. Core component doing the conversion is a character classifier, which is implemented using svm. Classifiers input is character glyph features which is obtained by extracting HOG feature from glyph. So lekha OCR accuracy depends on how well charters are extracted from image.  

-------------------------------
Layout analyzer analyses the the input image and finds paragraphs, later each paragraph is analyzed and words are extracted in their order. Then words are split into characters based on the spacing between each characters.  

-------------------------------
Lekha finds variables like average character length, word spacing, line spacing etc dynamically, these variables aids layout analyzer to find characters in an image. Some of the parameters for layout analyzer are not dynamic. So we have assumed that the image inputs for lekha will have certain fixed parameters like.
 * Images are scanned in 300 dpi
 * Noise in images are random
 * Image should contain at least 1 paragraph with 4 lines
 * No broken or joined glyphs with minimum  of 20 pixel width or height  
 
 
-------------------------------

 ![broken characters in image](images/bro_cut3.png)
 
When characters are broken as shown in above image, lekha assumes each broken component as a glyph converts it into a character. same goes with joined glyphs ie if 2 characters join due to a smudge or noise in image lekha reads it as a single glyph and produces its output.  


Solution to above problem is scan documents at or above 300 dpi, without any filters or operations such as white balance, thresholding applied. A spell checker is implemented to address broken, joined and misclassified character problem.   


Spell checker validation requires curated lexicon which is not available for all Malayalam words because of agglutinative property of words. We have extracted data from Wikipedia and a lexicon is made which is partially curated.  


By default spell check is turned of due to unavailability of good lexicon. if your image contains broken, joined chars turn the spell checker on which will improve results  


Lekha's modes of operation can be controlled via a conf.json file. After succesfull installation config.json will be located at ~/.lekha/lekha_ocr/ (path relative to user home directory)

-------------------------------
Candidates for spell corrector is found by two different algorithms 
1. Finding similar words by using similarity in word shapes -conf[spell]=2
2. Generate word candidates with 1 edit distance applied on character level conf[spell]=1
3. Turn off -conf[spell]=0
These modes can be selected in the configuration file  

-------------------------------
Lekha OCR uses 3 models to convert charter glyph to corresponding text  
1. Classifier trained on 400+ glyphs which include all Malayalam characters + english   - slow -conf[lekha]=0
2. Classifier trained on 192 glyphs which includes reformed script charters + english - fast- conf[lekha]=1
3. Classifier trained on 227 glyphs which occur in old Malayalam fonts + english       - fast - conf[lekha]=2

These modes can be selected in the configuration file  


-------------------------------
As svm doesn't scale well with increase in output classes as multi class svm is basically many binary classifiers ensembled as a result model parameters, model size will also increase.  

As output classes increases memory required to load classifier, classification time increases which affects lekha ocrs performance. So select model appropriately  

-------------------------------
Confusion matrix analysis of each model reveled another issue that is missclassifying similar shape charters like 'o','0','ഠ','ം' and Malayalam letter 'ട' with english letter 's'. This issue is solved by finding language of each word which uses number of characters belonging to language as metric to find target language and approximating each misclassified character to similar shape glyph in target language or number class.
This feature is by default turned on. 

To turn off conf[postprocess]=0
it can be turned off in the config file
