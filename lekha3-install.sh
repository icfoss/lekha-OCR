#installing python and pip3 for installing packages
sudo apt update
sudo apt-get install  build-essential pkg-config python-dev python3-dev  python3-pip python3-tk fonts-smc python3-gi python3-gi-cairo gir1.2-gtk-3.0 libgirepository1.0-dev gcc libcairo2-dev
pip3 install --upgrade pip

#echo "export VIRTUALENVWRAPPER_PYTHON=/usr/local/bin/python" >> $HOME/.bashrc
#echo "export VIRTUALENVWRAPPER_VIRTUALENV=/usr/local/bin/virtualenv" >> $HOME/.bashrc



sudo pip install virtualenv virtualenvwrapper

mkdir $HOME/.envs
mkdir $HOME/.lekha

echo "export WORKON_HOME=$HOME/.envs" >> $HOME/.bashrc
echo "export VIRTUALENVWRAPPER_PYTHON=/usr/bin/python" >> $HOME/.bashrc
echo "export VIRTUALENVWRAPPER_VIRTUALENV=/usr/local/bin/virtualenv" >> $HOME/.bashrc
echo "source /usr/local/bin/virtualenvwrapper.sh" >> $HOME/.bashrc

export WORKON_HOME=$HOME/.envs
export VIRTUALENVWRAPPER_PYTHON=/usr/bin/python
export VIRTUALENVWRAPPER_VIRTUALENV=/usr/local/bin/virtualenv
source /usr/local/bin/virtualenvwrapper.sh


mkvirtualenv pyt3 -p python3
workon pyt3
pip install pycairo opencv-python==3.4.3.18 pygobject  Pillow matplotlib scikit-image pyinsane2 pandas scikit-learn==0.20.0 vext vext.gi
deactivate pyt3

MYPWD=${PWD}
cp  -r $MYPWD/.  $HOME/.lekha

sudo rm lekha_ocr_ver3.desktop
sudo printf "[Desktop Entry]\n
Encoding=UTF-8\n
Version=1.0\n
Type=Application\n
Name=lekha ocr ver.3.0\n
Icon=$HOME/.lekha/lekha_ocr/lekhalogo.png\n
Path=$HOME/.lekha\n
Exec=bash desktop.sh\n
StartupNotify=true\n
StartupWMClass=__init__.py\n
Name[en_US]=lekha ocr ver3.0" >> lekha_ocr_ver3.desktop

sudo cp -f lekha_ocr_ver3.desktop /usr/share/applications/
sudo chmod 666 $HOME/.lekha/lekha_ocr/conf.json
echo "Lekha ocr ver 3.0 installed successfully"

#Run this below 4 lines and run  if your gtk version is less than 3.20 
#sudo add-apt-repository ppa:gnome3-team/gnome3-staging
#sudo add-apt-repository ppa:gnome3-team/gnome3
#sudo apt update
#sudo apt dist-upgrade

