# Lekha Ocr version 3.0


## Description

Lekha Ocr Converts your malayalam documents and images to editable malayalam text.It's designed to be easy,fast and simple to use.it has add-on features like scanning,croping,rotating and skew correction of images which help you to do things easy.

For detailed description about lekha and how to get better _results_. Refer
[Description](Description.md)


## Screenshots

### Main Window


![ScreenShot](screenshots/screenshot_mainwindow.png)

### Main Window after convertion

![ScreenShot](screenshots/screenshot_output.png)


### Layout Analyzed Image

![ScreenShot](screenshots/screenshot_layout.png)

### Crop

![ScreenShot](screenshots/screenshot_crop.png)



## Main features
* Scan
* Import image from disk
* Image layout analysis, features to edit layout results : draw new box by selecting and then press enter to confirm  ,delete the incorrect layout analysed box by clicking on respective box.
* Malayalam ocr (lekha_ocrv3)
* [Image Skew correction](https://github.com/kakul/Alyn)
* Rotate image
* Crop image : crop out unwanted parts of imported/scanned image using crop functionality.
* Save Output malayalam text to text file.
* Features to do spell-check based on word shape properties, character combinations

## Dependencies
* Debian 9
* Python3
* Opencv version== 3.4.3.18
* Gtk version >=3.20
* scikit-learn ==0.20.0
* Pillow
* vext
* vext.gi
* pygobject  
* matplotlib
* scikit-image
* pyinsane2
* pandas

## Details

It mainly uses:

* [Pyinsane2](https://pypi.python.org/pypi/pyinsane2): To scan the pages
* [OPENCV](https://github.com/opencv/opencv): for image manipulation and calculations
* [Lekha_ocrv2](https://github.com/space-kerala/LEKHA_OCR_VER2.0/tree/master/lekha_ocr)(OCR)
* [GTK+](http://www.gtk.org/): For the user interface
* [PYGOBJECT](https://pygobject.readthedocs.io/): python binding for Gtk+
* [Matplotlib](https://matplotlib.org/): Selection and for layout analyzed output correction features
* [Pillow](https://pypi.python.org/pypi/Pillow/)


## Installation : Debian

* git clone https://gitlab.com/icfoss/lekha-OCR
  (or Download zip and extract)
* Navigate to the cloned or extracted directory
* chmod +x lekha3-install.sh
* ./lekha3-install.sh
* installation done  
* Open the application by searching lekha ocr ver 3 in the dash


## To Uninstall
* chmod +x uninstall.sh
* ./uninstall.sh

## Creators  
* [Sachin Gracious](https://github.com/sachingracious)
* [Sajaras k](https://github.com/sajaras)
* [Yadhukrishnan K](https://github.com/yadu17)

## Project Guidance and Support
* Dr Rajeev R.R

## Contributors
* Ambily Sreekumar,contributed in building data set for training.
* [Jithin thankachan](https://github.com/jithin-space):contributed in training tool and helped in documentation.
* [Arun Joseph](https://github.com/arunjoseph0):contributed most of the engine initial  developments of [Lekha ocr 1.0](https://gitlab.com/space-kerala/lekha-OCR)
* Balagopal Unnikrishnan : Contributed in preparing XML label for training and helped in documentation of[Lekha ocr 1.0](https://gitlab.com/space-kerala/lekha-OCR)
* Rijoy V : Contributed in initial research of [Lekha OCR ver 1.0](https://gitlab.com/space-kerala/lekha-OCR)

## Developers

This project is a joint initiative of [SPACE-KERALA](http://www.space-kerala.org/) & [ICFOSS](https://icfoss.in)


## Licence

GPLv3 only. See License.md


## Development

All the information can be found on [ICFOSS-Gitlab](https://gitlab.com/icfoss/lekha-OCR)
