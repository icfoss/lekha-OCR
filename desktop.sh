#!/bin/bash
# source ~/.bashrc
export WORKON_HOME=$HOME/.envs
export VIRTUALENVWRAPPER_PYTHON=/usr/bin/python3
export VIRTUALENVWRAPPER_VIRTUALENV=/usr/local/bin/virtualenv
source /usr/local/bin/virtualenvwrapper.sh
workon pyt3
cd $HOME/.lekha/lekha_ocr
python3  __init__.py
deactivate pyt3
